package com.ping;

import com.ping.logger.Level;
import com.ping.logger.PingLogger;
import com.ping.resource.ReportHandler;
import com.ping.service.PingService;
import com.ping.service.impl.PingServiceImpl;
import com.sun.net.httpserver.HttpServer;

import java.io.IOException;
import java.net.InetSocketAddress;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.*;

import static com.ping.constants.Constants.*;

public class Application {

    private static final PingLogger logger = PingLogger.getInstance(Application.class.getSimpleName(), Level.WARNING);

    public static void main(String[] args) {

        PingService pingService = PingServiceImpl.getInstance();

        startReportServer();

        List<Callable<Boolean>> tasksICMP = new ArrayList<>();
        List<Callable<Boolean>> tasksTCP = new ArrayList<>();
        List<Callable<Boolean>> tasksTrace = new ArrayList<>();

        for (String url : args) {
            Callable<Boolean> taskICMP = () -> pingService.pingHostICMP(url);
            tasksICMP.add(taskICMP);
            Callable<Boolean> taskTCP = () -> pingService.pingHostTCP(url);
            tasksTCP.add(taskTCP);
            Callable<Boolean> taskTrace = () -> pingService.traceRouteHost(url);
            tasksTrace.add(taskTrace);
        }

        ExecutorService executorService = Executors.newFixedThreadPool(calculateOptimumThreadCount());

        ScheduledExecutorService executorIcmp = Executors.newScheduledThreadPool(calculateOptimumThreadCount());
        ScheduledExecutorService executorTrace = Executors.newScheduledThreadPool(calculateOptimumThreadCount());
        ScheduledExecutorService executorTcp = Executors.newScheduledThreadPool(calculateOptimumThreadCount());

        Runnable runnableICMP = () -> {
            try {
                executorService.invokeAll(tasksICMP);
            } catch (InterruptedException ex) {
                logger.log(Level.ERROR, ex.getMessage());
            }
        };

        Runnable runnableTCP = () -> {
            try {
                executorService.invokeAll(tasksTCP);
            } catch (InterruptedException ex) {
                logger.log(Level.ERROR, ex.getMessage());
            }
        };

        Runnable runnableTrace = () -> {
            try {
                executorService.invokeAll(tasksTrace);
            } catch (InterruptedException ex) {
                logger.log(Level.ERROR, ex.getMessage());
            }
        };

        executorTcp.scheduleAtFixedRate(runnableTCP, INIT_DELAY_TCP, SCHEDULE_PERIOD, TimeUnit.MILLISECONDS);
        executorIcmp.scheduleAtFixedRate(runnableICMP, INIT_DELAY_ICMP, SCHEDULE_PERIOD, TimeUnit.MILLISECONDS);
        executorTrace.scheduleAtFixedRate(runnableTrace, INIT_DELAY_TRACE, SCHEDULE_PERIOD, TimeUnit.MILLISECONDS);

    }

    private static void startReportServer() {
        HttpServer server;
        try {
            server = HttpServer.create(new InetSocketAddress(SERVER_PORT), SOCKET_BACKLOG);
            server.createContext(SERVER_CONTEXT_PATH, new ReportHandler());
            server.setExecutor(null);
            server.start();
        } catch (IOException ex) {
            logger.log(Level.ERROR, ex.getMessage());
        }
    }

    private static int calculateOptimumThreadCount() {
        int cores = Runtime.getRuntime().availableProcessors();
        return cores * (1 + 100 / 20);
    }

}
