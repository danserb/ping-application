package com.ping.logger;

public enum Level {

    DEBUG, ERROR, WARNING, INFO

}
