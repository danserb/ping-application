package com.ping.logger;

import java.time.LocalDateTime;

import static java.lang.System.out;

public class PingLogger {

    private static final String DASH = " - ";

    private static PingLogger pingLogger;

    private Level logLevel;
    private String loggerName;

    private PingLogger(String name, Level level) {
        this.loggerName = name;
        this.logLevel = level;
    }

    public static PingLogger getInstance(String name, Level level) {
        if (pingLogger == null) {
            pingLogger = new PingLogger(name, level);
        }
        return pingLogger;
    }

    public void log(Level level, String message) {
        if (level.equals(logLevel)) {
            out.println(formatLog(message));
        }
    }

    private String formatLog(String message) {
        return LocalDateTime.now() + DASH + loggerName + DASH + message;
    }

}