package com.ping.constants;

import java.util.Arrays;
import java.util.List;

public class Constants {
    public static final String SERVER_CONTEXT_PATH = "/api/reports";
    public static final String LOCAL_SERVER = "http://localhost:8080/api/reports";
    public static final int SERVER_PORT = 8080;
    public static final int SOCKET_BACKLOG = -1;

    public static final int INIT_DELAY_TRACE = 0;
    public static final int INIT_DELAY_TCP = 200;
    public static final int INIT_DELAY_ICMP = 400;
    public static final int SCHEDULE_PERIOD = 5000;

    public static final String REQUEST_TIMED_OUT = "Request Timed Out";

    public static final String POST = "POST";
    public static final String GET = "GET";
    public static final String CONTENT_TYPE = "Content-Type";
    public static final String CONTENT_LENGTH = "Content-Length";
    public static final String APPLICATION_JSON = "application/json; uft-8";
    public static final String CHARSET = "charset";
    public static final String UTF_8 = "utf-8";

    public static final int NO_BODY_LENGTH = -1;
    public static final int METHOD_NOT_ALLOWED = 405;
    public static final int OK = 200;

    public static final String TIMED_OUT = "timed out";
    public static final String NOT_REACHABLE = "not reachable";

    public static final String HTTP_PROTOCOL = "http://";
    public static final String HOST = "Host: ";
    public static final String RESPONSE_TIME = "Response time: ";
    public static final String STATUS_CODE = "Status code: ";
    public static final String MILISECONDS = "ms";

    public static final List<Integer> BAD_STATUS_CODES = Arrays.asList(
            401, 403, 404, 409, 410, 500, 502, 504);
}
