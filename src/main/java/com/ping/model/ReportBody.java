package com.ping.model;

public class ReportBody {

    private String host;
    private String icmp_Ping;
    private String tcp_Ping;
    private String trace;

    private ReportBody(String host, String icmp_Ping, String tcp_Ping, String trace) {
        this.host = host;
        this.icmp_Ping = icmp_Ping;
        this.tcp_Ping = tcp_Ping;
        this.trace = trace;
    }

    public String getHost() {
        return host;
    }

    public void setHost(String host) {
        this.host = host;
    }

    public String getIcmp_Ping() {
        return icmp_Ping;
    }

    public void setIcmp_Ping(String icmpPing) {
        this.icmp_Ping = icmpPing;
    }

    public String getTcp_Ping() {
        return tcp_Ping;
    }

    public void setTcp_Ping(String tcpPing) {
        this.tcp_Ping = tcpPing;
    }

    public String getTrace() {
        return trace;
    }

    public void setTrace(String trace) {
        this.trace = trace;
    }

    @Override
    public String toString() {
        return "{" +
                "host='" + host + '\'' +
                ", icmp_Ping='" + icmp_Ping + '\'' +
                ", tcp_Ping='" + tcp_Ping + '\'' +
                ", trace='" + trace + '\'' +
                '}';
    }

    public static Builder builder() {
        return new Builder();
    }

    public static class Builder {
        private String host;
        private String icmp_Ping;
        private String tcp_Ping;
        private String trace;

        public Builder withHost(String host) {
            this.host = host;
            return this;
        }

        public Builder withIcmpPing(String icmpPing) {
            this.icmp_Ping = icmpPing;
            return this;
        }

        public Builder withTcpPing(String tcpPing) {
            this.tcp_Ping = tcpPing;
            return this;
        }

        public Builder withTrace(String trace) {
            this.trace = trace;
            return this;
        }

        public ReportBody build() {
            return new ReportBody(host, icmp_Ping, tcp_Ping, trace);
        }

    }
}