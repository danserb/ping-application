package com.ping.resource;

import com.ping.service.FileWriterService;
import com.ping.service.impl.FileWriterServiceImpl;
import com.sun.net.httpserver.HttpExchange;
import com.sun.net.httpserver.HttpHandler;

import java.io.*;
import java.util.stream.Collectors;

import static com.ping.constants.Constants.*;

public class ReportHandler implements HttpHandler {

    private FileWriterService fileWriterService = FileWriterServiceImpl.getInstance();

    public ReportHandler() {
    }

    @Override
    public void handle(HttpExchange httpExchange) throws IOException {

        if (POST.equals(httpExchange.getRequestMethod())) {
            InputStream is = httpExchange.getRequestBody();
            String body = new BufferedReader(new InputStreamReader(is)).lines().collect(Collectors.joining("\n"));
            fileWriterService.writeResponseToFile(body);
        } else {
            httpExchange.sendResponseHeaders(METHOD_NOT_ALLOWED, NO_BODY_LENGTH);
        }
        httpExchange.sendResponseHeaders(OK, NO_BODY_LENGTH);
        OutputStream output = httpExchange.getResponseBody();
        output.flush();
        httpExchange.close();
    }

}
