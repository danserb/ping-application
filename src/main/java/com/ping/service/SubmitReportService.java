package com.ping.service;

import com.ping.model.ReportBody;

public interface SubmitReportService {

    void submitReport(ReportBody reportBody);

}
