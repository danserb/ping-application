package com.ping.service;

public interface FileWriterService {

    void writeResponseToFile(String response);

}
