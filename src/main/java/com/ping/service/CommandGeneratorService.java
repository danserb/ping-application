package com.ping.service;

public interface CommandGeneratorService {

    String buildPingCommandString(String host);

    String buildTraceCommandString(String host);

}
