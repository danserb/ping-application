package com.ping.service;

public interface PingService {

    Boolean pingHostICMP(String host);

    Boolean pingHostTCP(String host);

    Boolean traceRouteHost(String host);

}
