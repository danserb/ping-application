package com.ping.service.impl;

import com.ping.constants.Constants;
import com.ping.logger.Level;
import com.ping.logger.PingLogger;
import com.ping.model.ReportBody;
import com.ping.service.CommandGeneratorService;
import com.ping.service.PingService;
import com.ping.service.SubmitReportService;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.SocketTimeoutException;
import java.net.URL;
import java.time.LocalDateTime;
import java.util.Map;
import java.util.Objects;
import java.util.concurrent.ConcurrentHashMap;

import static com.ping.constants.Constants.*;

public class PingServiceImpl implements PingService {

    private static final PingLogger logger = PingLogger.getInstance(PingServiceImpl.class.getSimpleName(), Level.WARNING);

    private static PingServiceImpl instance;

    private CommandGeneratorService commandGeneratorService;
    private SubmitReportService submitReportService;

    private Map<String, String> icmpResponseMap = new ConcurrentHashMap<>();
    private Map<String, String> tcpResponseMap = new ConcurrentHashMap<>();
    private Map<String, String> tracertResponseMap = new ConcurrentHashMap<>();

    private PingServiceImpl(CommandGeneratorService commandGeneratorService,
                            SubmitReportService submitReportService) {
        this.commandGeneratorService = commandGeneratorService;
        this.submitReportService = submitReportService;
    }

    public static PingServiceImpl getInstance() {
        if (instance == null) {
            instance = new PingServiceImpl(
                    CommandGeneratorServiceImpl.getInstance(),
                    SubmitReportServiceImpl.getInstance());
        }
        return instance;
    }

    @Override
    public Boolean pingHostICMP(String host) {
        StringBuilder result = new StringBuilder();
        String command = commandGeneratorService.buildPingCommandString(host);
        Process process;
        try {
            process = Runtime.getRuntime().exec(command);
            BufferedReader input = new BufferedReader(new InputStreamReader(process.getInputStream()));
            while (Objects.nonNull(input.readLine())) {
                input.lines().forEach(t -> result.append("\n").append(t));
                if (result.toString().contains(TIMED_OUT) || result.toString().contains(NOT_REACHABLE)) {
                    submitReportService.submitReport(buildReportBody(host));
                }
            }
        } catch (Exception ex) {
            logger.log(Level.ERROR, ex.getMessage());
        }

        result.insert(0, LocalDateTime.now().toString());
        icmpResponseMap.put(host, result.toString());
        return Boolean.TRUE;

    }

    @Override
    public Boolean pingHostTCP(String host) {
        StringBuilder stringBuilder = new StringBuilder();
        URL url;
        try {
            url = new URL(HTTP_PROTOCOL + host);
            HttpURLConnection conn = (HttpURLConnection) url.openConnection();
            conn.setRequestMethod(GET);
            conn.setConnectTimeout(1000);
            long startingTime = System.currentTimeMillis();
            conn.connect();
            int status = conn.getResponseCode();
            long responseTime = System.currentTimeMillis() - startingTime;

            if (BAD_STATUS_CODES.contains(status)) {
                submitReportService.submitReport(buildReportBody(host));
            }

            stringBuilder
                    .append(LocalDateTime.now().toString()).append("\n")
                    .append(HOST).append(host).append("\n")
                    .append(RESPONSE_TIME).append(responseTime)
                    .append(MILISECONDS).append("\n")
                    .append(STATUS_CODE).append(status).append("\n");

            tcpResponseMap.put(host, stringBuilder.toString());

        } catch (SocketTimeoutException se) {
            logger.log(Level.ERROR, se.getMessage());
            submitReportService.submitReport(buildReportBody(host));
        } catch (IOException ex) {
            logger.log(Level.ERROR, ex.getMessage());
        }
        return Boolean.TRUE;
    }

    @Override
    public Boolean traceRouteHost(String host) {
        StringBuilder result = new StringBuilder();
        String command = commandGeneratorService.buildTraceCommandString(host);
        Process process;
        try {
            process = Runtime.getRuntime().exec(command);
            BufferedReader input = new BufferedReader(new InputStreamReader(process.getInputStream()));
            while (Objects.nonNull(input.readLine())) {
                input.lines().forEach(t -> result.append("\n").append(t));
                if (result.toString().contains(Constants.REQUEST_TIMED_OUT)) {
                    submitReportService.submitReport(buildReportBody(host));
                }
            }
        } catch (IOException ex) {
            logger.log(Level.ERROR, ex.getMessage());
        }

        result.insert(0, LocalDateTime.now().toString());
        tracertResponseMap.put(host, result.toString());
        return Boolean.TRUE;
    }

    private ReportBody buildReportBody(String host) {
        return ReportBody.builder()
                .withHost(host)
                .withIcmpPing(icmpResponseMap.get(host) == null ? "No Valid Response Yet" : icmpResponseMap.get(host))
                .withTcpPing(tcpResponseMap.get(host)  == null ? "No Valid Response Yet" : tcpResponseMap.get(host))
                .withTrace(tracertResponseMap.get(host)  == null ? "No Valid Response Yet" : tracertResponseMap.get(host))
                .build();
    }

}
