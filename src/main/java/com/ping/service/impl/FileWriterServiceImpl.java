package com.ping.service.impl;

import com.ping.service.FileWriterService;

import java.io.FileWriter;
import java.io.IOException;

public class FileWriterServiceImpl implements FileWriterService {

    private static FileWriterServiceImpl instance;

    private static final String LOG_FILE_PATH = "logs/logs.txt";

    private FileWriterServiceImpl() {
    }

    public static FileWriterServiceImpl getInstance() {
        if (instance == null) {
            instance = new FileWriterServiceImpl();
        }
        return instance;
    }

    @Override
    public void writeResponseToFile(String response) {
//        URL resource = this.getClass().getClassLoader().getResource(LOG_FILE_PATH);
//        Objects.requireNonNull(resource);
        try (FileWriter fileWriter = new FileWriter("test.log", true)) {
            fileWriter.write(response.concat("\n"));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

}
