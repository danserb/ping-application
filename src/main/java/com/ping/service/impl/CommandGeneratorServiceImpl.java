package com.ping.service.impl;

import com.ping.service.CommandGeneratorService;

public class CommandGeneratorServiceImpl implements CommandGeneratorService {

    private static CommandGeneratorServiceImpl commandGeneratorService;

    private CommandGeneratorServiceImpl() {
    }

    static CommandGeneratorServiceImpl getInstance() {
        if (commandGeneratorService == null) {
            commandGeneratorService = new CommandGeneratorServiceImpl();
        }
        return commandGeneratorService;
    }

    private static final String PING_COMMAND_WIN = "ping -n 5 ";
    private static final String PING_COMMAND_LINUX = "ping -c 5 ";
    private static final String TRACE_COMMAND_WIN = "tracert ";
    private static final String TRACE_COMMAND_LINUX = "traceroute -I ";

    @Override
    public String buildPingCommandString(String host) {
        String command;
        if (System.getenv("os").startsWith("Windows")) {
            command = PING_COMMAND_WIN.concat(host);
        } else {
            command = PING_COMMAND_LINUX.concat(host);
        }
        return command;
    }

    @Override
    public String buildTraceCommandString(String host) {
        String command;
        if (System.getenv("os").startsWith("Windows")) {
            command = TRACE_COMMAND_WIN.concat(host);
        } else {
            command = TRACE_COMMAND_LINUX.concat(host);
        }
        return command;
    }
}
