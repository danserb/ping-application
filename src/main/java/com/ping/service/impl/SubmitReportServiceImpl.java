package com.ping.service.impl;

import com.ping.constants.Constants;
import com.ping.logger.Level;
import com.ping.logger.PingLogger;
import com.ping.model.ReportBody;
import com.ping.service.SubmitReportService;

import java.io.IOException;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.nio.charset.StandardCharsets;

import static com.ping.constants.Constants.*;

public class SubmitReportServiceImpl implements SubmitReportService {

    private PingLogger logger = PingLogger.getInstance(SubmitReportServiceImpl.class.getSimpleName(), Level.WARNING);
    private static SubmitReportServiceImpl submitReportService;


    private SubmitReportServiceImpl(){}

    public static SubmitReportServiceImpl getInstance() {
        if (submitReportService == null) {
            submitReportService = new SubmitReportServiceImpl();
        }
        return submitReportService;
    }

    @Override
    public void submitReport(ReportBody reportBody) {
        URL url;
        try {
            url = new URL(Constants.LOCAL_SERVER);
            HttpURLConnection conn = (HttpURLConnection) url.openConnection();
            conn.setRequestMethod(POST);
            conn.setRequestProperty(CONTENT_TYPE, APPLICATION_JSON);
            conn.setDoOutput(true);
            conn.setRequestProperty(CHARSET, UTF_8);
            conn.setRequestProperty(CONTENT_LENGTH, Integer.toString(reportBody.toString().length()));
            conn.setUseCaches(false);

            try(OutputStream os = conn.getOutputStream()) {
                byte[] input = reportBody.toString().getBytes(StandardCharsets.UTF_8);
                os.write(input, 0, input.length);
                //trigger call
                conn.getResponseCode();
            }
        } catch (IOException ex) {
            logger.log(Level.ERROR, ex.getMessage());
        }

    }
}
